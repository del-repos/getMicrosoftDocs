# getMicrosoftDocs
A Web Crawler that is able to parse thru the Microsoft Docs for Code references 

# ChromeDriver

You might need a need another chromedriver that isn't part of the bin: https://chromedriver.storage.googleapis.com/index.html

# Usage:
```sh
# Change Directory to getMicrosoftDocs
cd getMicrosoftDocs/

# Setup the virtual environment for python 
python3 -m venv .

# Activate virtual environment 
source bin/activate

# Install python packages (pip packages)
python3 -m pip install -r requirements.txt

# Run script
python3 validateAPI.py

# To deactivate the virtual environment
deactivate
```



