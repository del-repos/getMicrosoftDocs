import time
import platform
import os
import requests
from bs4 import BeautifulSoup  #beautifulsoup4
from selenium import webdriver   # selenium
from selenium.webdriver.chrome.options import Options  #selenium
import networkx as nx # networkx
import debug as d


# https://chromedriver.storage.googleapis.com/index.html?path=2.42/

DOCS = "https://docs.microsoft.com/"


android_api = lambda x: f"https://docs.microsoft.com/en-us/dotnet/api/?term={x}"

# Allows validateAPI.py script to be run on multiple Operating Systems
def valid_chromedriver():
    end = ""

    system = platform.system()

    if(system == "Darwin"):
        end = "mac"
    elif(system == "Linux" ):
        end = "linux"
    elif(system == "Windows"):
        end = "win32.exe"
    return f"{os.getcwd()}/binaries/chromedriver-{end}"

# Most Stolen from https://stackoverflow.com/questions/52687372/beautifulsoup-not-returning-complete-html-of-the-page
# SeleniumDrive(): just makes sure you can get all the content from a website (even if it runs dynamically)
def seleniumDrive(url):
    chromedriver_path = f"{valid_chromedriver()}"
    # d.pr(chromedriver_path)

    # The Options section is so it doesn't open chrome or use the gpu
    options = Options()

    options.add_argument('--headless')
    options.add_argument('-disable-gpu')
    options.add_argument("--log-level=3")

    if(platform.system() == "Windows"):
        options.binary_location = "C:\Program Files\Google\Chrome\Application\chrome.exe"
        options.add_experimental_option('excludeSwitches', ['enable-logging'])

    driver = webdriver.Chrome(chromedriver_path, options=options)

    driver.get(url)

    time.sleep(3)

    page = driver.page_source

    driver.quit()

    return page

# Parses the soup object into useful information
def soupParse(html):
    
    soup = BeautifulSoup(html, 'html.parser')

    result = soup.find(id='api-browser-results-container')

    noExistence = soup.find("div", {"class":"no-results"})

    if(noExistence != None):
        return False  # Will result False if no results is on the site
    else:
        retDict = dict()

        deepParse = soup.find_all("td")

        for i in range(0, len(deepParse), 2):
            # res = deepParse[i]

            name = deepParse[i].text
            name = removeTypeInName(name)
            name = name.lower()

            explanation = deepParse[i + 1].text

            retDict[name] = explanation
            # for el in link:
            #     loso = el['href']
            #     los = f"https://docs.microsoft.com{loso}"
            #     d.pr(los)

        return retDict

# Removes extra info that is not needed
def removeTypeInName(name):
    # Remove the following:
    # Class Constructor Method Property 
    name = name.replace(" Class", "")
    name = name.replace(" Constructor", "")
    name = name.replace(" Method", "")
    name = name.replace(" Property", "")
    return name

# Orchestas all the web crawler code
def search(query, onlyDescription):
    query = query.lower()
    api = android_api(query)
    sd = seleniumDrive(api)

    contents = soupParse(sd)

    # OnlyDesciption is a boolean value that returns the value of the description of API
    if(onlyDescription and contents != False):
        return contents[query]
    
    return contents

# Orchestas all code to do a double check in searching
def orchesta(searchTerm, onlyDescription):

    firstSearch = search(searchTerm, onlyDescription)

    if(firstSearch == False):
        methodTerm = searchTerm.split(".")[-1]
        secondSearch = search(methodTerm, onlyDescription)

        if(secondSearch == False):
            return False
        else:
            return secondSearch

    return firstSearch

def orchesta(searchTerm, onlyDescription):
    searchTerm = searchTerm.lower()

    firstSearch = search(searchTerm, onlyDescription)

    if(firstSearch == False):
        methodTerm = searchTerm.split(".")[-1]
        secondSearch = search(methodTerm, onlyDescription)

        if(secondSearch == False):
            return False
        else:
            # TODO: Find quick way to extract text
            return secondSearch

    try:
        return firstSearch[searchTerm] 
    except KeyError:
        return firstSearch



if __name__ == "__main__":

    # # Double Check search example
    # content = main("dalvik.system.DexClassLoader", False)
    # print(content)

    # # Single Check Search Example
    # content = main("java.io.BufferedOutputStream", False)
    # print(content)


    # trial = ["java.io.BufferedOutputStream"] # ["asdfasdfasd", "java.io.BufferedOutputStream"]

    # for tri in trial:


        # To get the description of the API
        # result = main(tri, True)
        # d.pr(result)

        # # To get the result of the search of the API
        # result = main(tri, True)
        # d.pr(result)

    pass